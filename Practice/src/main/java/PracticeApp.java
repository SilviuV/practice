public class PracticeApp {
    public static void main(String[] args) {
        System.out.println("-* Basketball tickets App *-");

        TicketTypeProvider ticketTypeProvider = new TicketTypeProvider();
        ticketTypeProvider.printTicketsType();
        String selectedTicket = ticketTypeProvider.getTicketType();
        System.out.println("\nSelected ticket type: " + selectedTicket);

        Teams opponentTeam = new Teams();
        opponentTeam.printAvailableGames();
        opponentTeam.selectOpponentTeam();
        System.out.println("\nYou selected the game against: " + opponentTeam.getOpponent());

        Customer customer = new Customer();
        System.out.println("Customer full name is: " + customer.getName());

        System.out.println(customer.getName() + " has selected a " + selectedTicket + " ticket, against: " + opponentTeam.getOpponent());
    }
}
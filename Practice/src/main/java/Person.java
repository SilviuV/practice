import java.util.Scanner;

public class Person {

    private String firstName;
    private String lastName;

    public Person() {
        while (firstName == null || lastName == null) {
            if (firstName == null) {
                this.firstName = readUserInput("Insert first name");
            }
            if (lastName == null) {
                this.lastName = readUserInput("Insert last name");
            }
            if (this.firstName.isBlank()) {
                this.firstName = null;
            }
            if (this.lastName.isBlank()) {
                this.lastName = null;
            }
        }
    }

    private String readUserInput(String message) {
        Scanner keyboard = UserInput.getKeyboard();
        System.out.println(message);
        return keyboard.nextLine();
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }
}
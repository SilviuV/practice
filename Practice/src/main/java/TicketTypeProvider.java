import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class TicketTypeProvider {
    private List<String> knownTickets = new ArrayList<>();

    public TicketTypeProvider() {
        String homeTicket = "Home";
        String awayTicket = "Away";
        knownTickets.add(homeTicket);
        knownTickets.add(awayTicket);
    }

    public void printTicketsType() {
        System.out.println("Selected ticket type: ");
        for (String knownTickets : knownTickets) {
            System.out.print(knownTickets + ", ");
        }
        System.out.println("");
    }

    public String getTicketType() {
        Scanner keyboard = UserInput.getKeyboard();
        String input = keyboard.nextLine();
        for (String knownTickets : knownTickets) {
            if (knownTickets.equalsIgnoreCase(input))
                return input;
        }
        return "Unknown selection";
    }
}

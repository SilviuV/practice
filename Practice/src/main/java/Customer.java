public class Customer extends Person {

    public Customer() {
        super();
    }

    public String getName() {
        return super.getFirstName() + " " + super.getLastName();
    }
}

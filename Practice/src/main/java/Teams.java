import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Teams {

    private String opponent;

    private final List<String> opponents = new ArrayList<>();

    public Teams() {
        String cluj = "1. U-BT Cluj-Napoca";
        String sibiu = "2. CSU Sibiu";
        String oradea = "3. CSM Oradea";
        String timisoara = "4. SCM Timisoara";
        opponents.add(cluj);
        opponents.add(sibiu);
        opponents.add(oradea);
        opponents.add(timisoara);
    }

    public void printAvailableGames() {
        System.out.println("Available games are against: ");
        for (String opponent : opponents) {
            System.out.println(opponent);
        }
    }

    public void selectOpponentTeam() {
        Scanner keyboard = UserInput.getKeyboard();
        String input = keyboard.nextLine();
        for (String o : opponents) {
            if (o.startsWith(input)) {
                this.opponent = o;
            }
        }
    }

    public String getOpponent() {
        return opponent;
    }
}
